# fluid-simulation

![ALT](./screenshoots/elephant.jpg)

__dependencies:__ *sdl2*, *sdl2_ttf*, *c++ toolchain*   

## SDL installation instructions   
If you are on a debian system   
`sudo apt-get install libsdl2-dev libsdl2-ttf-dev`  

Or an OSX system   
`brew install sdl2`   
`brew install sdl2_ttf`   

### ScreenShoot n1
![ALT](./screenshoots/01.jpg)
### ScreenShoot n2
![ALT](./screenshoots/02.jpg)

## Compilation   
`make`

## Execution   
`./mod1`
